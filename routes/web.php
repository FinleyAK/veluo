<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/', 'RouteController@index')->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/hub', 'RouteController@hub')->name('hub')->middleware('auth');


// User
Route::prefix('/settings')->group(function () {
    Route::get('/', 'UserController@index');
    Route::get('/profile', 'UserController@profile')->name('profile');
});


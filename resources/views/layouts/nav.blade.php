<nav class="navbar is-transparent is-bold" role="navigation" aria-label="main navigation">
    <div class="container">
        <div class="navbar-brand">
            <a class="navbar-item has-text-primary" href="{{ route('index') }}">
                <strong>Web Server</strong>
            </a>

            <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="veluo">
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
                <span aria-hidden="true"></span>
            </a>
        </div>
        <div id="veluo" class="navbar-menu">
            <div class="navbar-start">
                <a class="navbar-item has-text-primary" href="{{ route('hub') }}">Home</a>
            </div>
            <div class="navbar-end">
                @auth
                <a class="navbar-item has-text-primary" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        Logout, {{ Auth::user()->name }}</a>
                     </div>
                @else

                <a class="navbar-item has-text-primary" href="{{ route('login') }}">
                    Login
                </a>
                <a class="navbar-item has-text-primary" href="{{ route('register') }}">
                    Register
                </a>
            </div>

            @endif
        </div>

        <form id="logout-form" action="{{ route('logout') }}" method="POST"
        style="display: none;">
        {{ csrf_field() }}
     </form>
    </div>
    </div>
</nav>

@section ('js')
<script type="text/javascript">
    $(function () {
        $('.navbar-burger').click(function () {
            $(".navbar-burger").toggleClass("is-active")
            $(".navbar-menu").toggleClass("is-active")
        })
    })

</script>

@endsection


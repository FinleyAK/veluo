
  @php use App\user; @endphp
@extends ('layouts.frame')
@section ('title', 'Profile')
@section ('content')

<section class="hero is-dark is-fullheight">
    <div class="hero-head">@include ('layouts.nav')</div>
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="card">
                <div class="card-image">
                  <figure class="image is-4by3">
                    <img src="{{ Auth::user()->avatar }}" alt="Placeholder image">
                  </figure>
                </div>
                <div class="card-content">
                  <div class="media">
                    <div class="media-left">
                      <figure class="image is-48x48">
                        <img src="https://bulma.io/images/placeholders/96x96.png" alt="Placeholder image">
                      </figure>
                    </div>
                    <div class="media-content">
                      <p class="title is-4">John Smith</p>
                      <p class="subtitle is-6">@johnsmith</p>
                    </div>
                  </div>

                  <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Phasellus nec iaculis mauris. <a>@bulmaio</a>.
                    <a href="#">#css</a> <a href="#">#responsive</a>
                    <br>
                    <time datetime="2016-1-1">11:09 PM - 1 Jan 2016</time>
                  </div>
                </div>
              </div>
        </div>
    </div>

</section>

    @endsection


  @php use App\user; @endphp
  @extends ('layouts.frame')
  @section ('title', 'Profile')
  @section ('content')
<section class="hero is-medium">
    <div class="hero-head">@include ('layouts.nav')</div>
</section>
<section class="hero is-dark is-fullheight">
    {{-- <tr>
       <td><strong>{{ Auth::user()->name }}</strong></td>
       <td>{{ Auth::user()->email }}</td>
       <td>{{ Auth::user()->userid }}</td>
    </tr> --}}
    <div class="field is-horizontal">
            <div class="field-label is-normal">
                <label class="label has-text-white">Name</label>
            </div>
            <div class="field-body">
                <div class="field is-expanded">
                    <div class="field has-addons">
                        <p class="control">
                            <input class="input" type="text" name="username" value="{{ Auth::user()->name }}">
                        </p>
                    </div>
                    <p class="help">Change your account username here. Note: Your discriminator (the four-digit number) will be changed if you update your username!</p>
                </div>
            </div>
        </div>

</section>
@endsection

@php use App\user; @endphp
@extends ('layouts.frame')
@section ('title', 'Home')
@section ('content')

<section class="hero is-dark is-fullheight">
    <div class="hero-head">@include ('layouts.nav')</div>
    <div class="hero-body">
        <div class="container has-text-centered"></div>
    </div>

</section>
    @endsection

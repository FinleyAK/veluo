@extends('layouts.frame') @section('title', 'Register') @section('content')
<section class="hero is-dark is-fullheight">
    <div class="hero-head">@include ('layouts.nav')</div>
    <div class="hero-body">
        <div class="column is-one-third is-offset-one-third">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="login-form">
                    <p class="control has-icon has-icon-right">
                        <span class="icon user">
                            <i class="fa fa-user"></i>
                        </span>
                        <small>Name</small>

                        <input class="input email-input" id="name" type="text" placeholder="name" name="name" value="{{ old('name') }}" required="required" autofocus="autofocus"> @if ($errors->has('name'))
                        <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span> @endif
                    </p>

                    <p class="control has-icon has-icon-right">
                        <span class="icon envelope">
                                <i class="fa fa-envelope"></i>
                            </span>
                        <small>Email</small>

                        <input id="email" type="text" class="input email-input" name="email" placeholder="johnsmith@gmail.com" value="{{ old('email') }}" required="required" autofocus="autofocus"> @if ($errors->has('email'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> @endif
                    </p>

                    <p class="control has-icon has-icon-right">
                        <span class="icon user">
                                    <i class="fa fa-lock"></i>
                                </span>
                        <small>Password</small>

                        <input id="password" type="password" class="input password-input" placeholder="password" name="password" required="required"> @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                    </p>



                    <p class="control has-icon has-icon-right">
                        <span class="icon user">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                        <small>Confirm Password</small>

                        <input id="password-confirm" type="password" class="input password-input" placeholder="confirm password" name="password_confirmation" required="required"> @if ($errors->has('password'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span> @endif


                                        <p class="control has-icon has-icon-right">
                                                <span class="icon envelope">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                <small>Avatar</small>

                                                <input id="avatar" type="avatar" class="input" name="avatar" placeholder="imgur.com/myawesomepicture" value="{{ old('avatar') }}" required="required" autofocus="autofocus"> @if ($errors->has('avatar'))
                                                <span class="help-block">
                                                            <strong>{{ $errors->first('avatar') }}</strong>
                                                        </span> @endif
                                            </p>



                        <p class="control has-icon has-icon-right">
                            <span class="icon user">
                                                <i class="fa fa-lock"></i>
                                            </span>
                            <small>UUID</small>
                            <input id="userid" type="string" class="input" placeholder="UserID" name="userid" value="{{ Uuid::generate()->string }}"  readonly="readonly"></input>
                            @if ($errors->has('userid'))
                            <span class="help-block">
                                                <strong>{{ $errors->first('userid') }}</strong>
                                            </span> @endif
                        </p>
                    </p>
                    <br/>
                    <p class="control login">
                        <button class="button is-success is-outlined is-large is-fullwidth" type="submit">Register</button>
                    </p>
                </div>
            </form>
        </div>
    </div>
</section>@endsection
